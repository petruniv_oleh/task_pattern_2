package com.oleh.kanban.model;

import com.oleh.kanban.model.kanbanImpl.ToDoState;

public class Task {
    private CurrentState currentState;
    private KanbanState state;
    private String title;
    private String explanation;
    private String developer;
    private String reviewer;
    private String reviewRes;

    public Task() {
        state = new ToDoState();
    }

    public Task(String title, String explanation) {
        state = new ToDoState();
        currentState = CurrentState.TODO;
        this.title = title;
        this.explanation = explanation;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getExplanation() {
        return explanation;
    }

    public void setExplanation(String explanation) {
        this.explanation = explanation;
    }

    public String getDeveloper() {
        return developer;
    }

    public void setDeveloper(String developer) {
        this.developer = developer;
    }

    public String getReviewer() {
        return reviewer;
    }

    public void setReviewer(String reviewer) {
        this.reviewer = reviewer;
    }

    public String getReviewRes() {
        return reviewRes;
    }

    public void setReviewRes(String reviewRes) {
        this.reviewRes = reviewRes;
    }

    public void setState(KanbanState state) {
        this.state = state;
    }

    public CurrentState getCurrentState() {
        return currentState;
    }

    public void setCurrentState(CurrentState currentState) {
        this.currentState = currentState;
    }

    public void toDo() {
        state.toDo(this);

    }

    public void inProgress() {
        state.inProgress(this);

    }

    public void review() {
        state.codeReview(this);

    }

    public void done() {
        state.done(this);

    }

    @Override
    public String toString() {
        return "Task: " +
                title + '\n' + explanation + '\n' +
                "current state: "+currentState+'\n'+
                "developer: " + developer + '\n' +
                "reviewer: " + reviewer + '\n' +
                "review message: " + reviewRes + '\n';
    }
}
