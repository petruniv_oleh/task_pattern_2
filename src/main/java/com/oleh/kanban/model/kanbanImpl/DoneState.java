package com.oleh.kanban.model.kanbanImpl;

import com.oleh.kanban.model.KanbanState;
import com.oleh.kanban.model.Task;

public class DoneState implements KanbanState {
    @Override
    public void done(Task task) {
        System.out.println("task "+task.getTitle()+" is already done!");
    }
}
