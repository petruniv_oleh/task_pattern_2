package com.oleh.kanban.model.kanbanImpl;

import com.oleh.kanban.model.CurrentState;
import com.oleh.kanban.model.KanbanState;
import com.oleh.kanban.model.Task;

public class InProgressState implements KanbanState {

    @Override
    public void inProgress(Task task) {
        System.out.println("task "+task.getTitle()+" is already in progress!");
    }

    @Override
    public void codeReview(Task task) {
        task.setState(new CodeReviewState());
        task.setCurrentState(CurrentState.REVIEW);
        task.setReviewRes(null);
        System.out.println("Task "+task.getTitle()+" is under review now!");
    }
}
