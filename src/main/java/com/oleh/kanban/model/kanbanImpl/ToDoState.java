package com.oleh.kanban.model.kanbanImpl;

import com.oleh.kanban.model.CurrentState;
import com.oleh.kanban.model.KanbanState;
import com.oleh.kanban.model.Task;

public class ToDoState implements KanbanState {

    @Override
    public void toDo(Task task) {
        System.out.println("task "+task.getTitle()+" is already in toDo list!");
    }

    @Override
    public void inProgress(Task task) {
        task.setState(new InProgressState());
        task.setCurrentState(CurrentState.IN_PROGRESS);
        System.out.println("Task "+task.getTitle()+" is in progress now!");
    }
}
