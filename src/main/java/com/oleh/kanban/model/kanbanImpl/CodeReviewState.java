package com.oleh.kanban.model.kanbanImpl;

import com.oleh.kanban.model.CurrentState;
import com.oleh.kanban.model.KanbanState;
import com.oleh.kanban.model.Task;

public class CodeReviewState implements KanbanState {

    @Override
    public void codeReview(Task task) {
        System.out.println("task "+task.getTitle()+" is already under review!");
    }

    @Override
    public void done(Task task) {
        task.setState(new DoneState());
        task.setCurrentState(CurrentState.DONE);
        System.out.println("Task "+task.getTitle()+" is done now!");
    }

    @Override
    public void toDo(Task task) {
        task.setState(new ToDoState());
        task.setCurrentState(CurrentState.TODO);
        System.out.println("Task "+task.getTitle()+" is int toDo list now!");
    }
}
