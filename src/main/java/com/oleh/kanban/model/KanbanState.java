package com.oleh.kanban.model;

public interface KanbanState {
    default void toDo(Task task){
        System.out.println("you can`t do this wright now");
    }

    default void inProgress(Task task){
        System.out.println("you can`t do this wright now");
    }

    default void codeReview(Task task){
        System.out.println("you can`t do this wright now");
    }

    default void done(Task task){
        System.out.println("you can`t do this wright now");
    }
}
