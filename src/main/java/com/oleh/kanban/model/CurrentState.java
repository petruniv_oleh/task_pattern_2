package com.oleh.kanban.model;

public enum CurrentState {
    TODO, IN_PROGRESS, REVIEW, DONE;
}
