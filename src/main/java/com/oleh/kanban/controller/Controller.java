package com.oleh.kanban.controller;


import com.oleh.kanban.model.Task;

import java.util.Map;

public interface Controller {

    void addTask(String title, String description);
    void moveToInProgress(String title, String devName);
    void moveToReview(String title, String reviewerName);
    void moveToDone(String title, String message);
    void moveToDo(String title, String message);
    Map<String, Task> getTaskMap();
}
