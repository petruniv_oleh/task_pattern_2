package com.oleh.kanban.controller;

import com.oleh.kanban.model.CurrentState;
import com.oleh.kanban.model.Task;

import java.util.LinkedHashMap;
import java.util.Map;

public class ControllerImpl implements Controller {

    private Map<String, Task> taskMap;

    public ControllerImpl() {
        taskMap = new LinkedHashMap<>();
    }


    @Override
    public void addTask(String title, String description) {
        taskMap.put(title, new Task(title, description));
    }

    @Override
    public void moveToInProgress(String title, String devName) {
        Task task = taskMap.get(title);
        if (task.getCurrentState() == CurrentState.TODO) {
            task.setDeveloper(devName);
        }
        task.inProgress();
    }

    @Override
    public void moveToReview(String title, String reviewerName) {
        Task task = taskMap.get(title);
        if (task.getCurrentState() == CurrentState.IN_PROGRESS) {
            task.setReviewer(reviewerName);
        }
        task.review();
    }

    @Override
    public void moveToDone(String title, String message) {
        Task task = taskMap.get(title);
        if (task.getCurrentState() == CurrentState.REVIEW) {
            task.setReviewRes(message);
        }
        task.done();
    }

    @Override
    public void moveToDo(String title, String message) {
        Task task = taskMap.get(title);
        if (task.getCurrentState() == CurrentState.REVIEW) {
            task.setReviewRes(message);
        }
        task.toDo();
    }

    @Override
    public Map<String, Task> getTaskMap() {
        return taskMap;
    }
}
