package com.oleh.kanban.view;

@FunctionalInterface
public interface Printable {
    void print() throws IllegalAccessException;
}
