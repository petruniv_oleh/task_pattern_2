package com.oleh.kanban.view;

public interface View {
    void showMenu();
    void addTask();
    void moveTaskToInProgress();
    void moveTaskToReview();
    void moveTaskToDone();
    void moveTaskToToDo();
    void printTasks();
}
