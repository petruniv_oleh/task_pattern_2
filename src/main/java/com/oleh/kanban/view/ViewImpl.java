package com.oleh.kanban.view;

import com.oleh.kanban.controller.Controller;
import com.oleh.kanban.controller.ControllerImpl;
import com.oleh.kanban.model.Task;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;


public class ViewImpl implements View{

    Controller controller;
    private Map<String, String> menuMap;
    private Map<String, Printable> menuMapMethods;
    private static Logger logger = LogManager.getLogger("com.oleh.kanban.view.ViewImpl");
    Scanner scanner = new Scanner(System.in);

    public ViewImpl() {
        controller = new ControllerImpl();
        menuMap();
        showMenu();
    }

    public void menuMap() {
        menuMap = new LinkedHashMap<>();
        menuMapMethods = new LinkedHashMap<>();
        menuMap.put("1", "1. Add new task");
        menuMap.put("2", "2. Move task to 'inProgress'");
        menuMap.put("3", "3. Move task to 'review'");
        menuMap.put("4", "4. Move task to 'done'");
        menuMap.put("5", "5. Move task to 'toDo'");
        menuMap.put("6", "6. Print all tasks");


        menuMapMethods.put("1", this::addTask);
        menuMapMethods.put("2", this::moveTaskToInProgress);
        menuMapMethods.put("3", this::moveTaskToReview);
        menuMapMethods.put("4", this::moveTaskToDone);
        menuMapMethods.put("5", this::moveTaskToToDo);
        menuMapMethods.put("6", this::printTasks);


    }

    private void mapMenuOut() {
        logger.info("Menu out");
        System.out.println("\nMenu:");
        for (String s : menuMap.values()
        ) {
            System.out.println(s);
        }
    }

    @Override
    public void showMenu() {
        String key;
        do {
            mapMenuOut();
            System.out.println("====================");
            System.out.println("Select option:");
            key = scanner.nextLine().toUpperCase();
            logger.info("The key for a menu is " + key);
            try {
                menuMapMethods.get(key).print();
            } catch (Exception e) {
                logger.error("Some problem with the key");
            }
        } while (!key.equals("Q"));
    }

    @Override
    public void addTask() {
        logger.debug("Invoke addTask");
        System.out.println("Enter title of new task (it should be unique!): ");
        String title = scanner.nextLine();
        System.out.println("Write some description of the task: ");
        String description = scanner.nextLine();

        logger.debug("Title '"+title+"' description '"+description+"'");

        controller.addTask(title, description);
    }

    @Override
    public void moveTaskToInProgress() {
        logger.debug("Invoke moveTaskToInProgress");

        System.out.println("Enter the title the task ");
        String title = scanner.nextLine();
        System.out.println("Enter the name of developer of the task ");
        String devName = scanner.nextLine();

        logger.debug("Title '"+title+"' devName '"+devName+"'");
        controller.moveToInProgress(title, devName);
    }

    @Override
    public void moveTaskToReview() {
        logger.debug("Invoke moveTaskToReview");

        System.out.println("Enter the title the task ");
        String title = scanner.nextLine();
        System.out.println("Enter the name of reviewer of the task ");
        String reviewer = scanner.nextLine();
        logger.debug("Title '"+title+"' reviewer '"+reviewer+"'");
        controller.moveToReview(title, reviewer);
    }

    @Override
    public void moveTaskToDone() {
        logger.debug("Invoke moveTaskToDone");

        System.out.println("Enter the title the task ");
        String title = scanner.nextLine();
        System.out.println("Enter the review message ");
        String message = scanner.nextLine();

        logger.debug("Title '"+title+"' message '"+message+"'");

        controller.moveToDone(title, message);
    }

    @Override
    public void moveTaskToToDo() {
        logger.debug("Invoke moveTaskToToDo");

        System.out.println("Enter the title the task ");
        String title = scanner.nextLine();
        System.out.println("Enter the review message ");
        String message = scanner.nextLine();

        logger.debug("Title '"+title+"' message '"+message+"'");

        controller.moveToDo(title, message);
    }

    @Override
    public void printTasks() {
        logger.debug("Invoke printTasks");

        Map<String, Task> taskMap = controller.getTaskMap();
        Set<String> keys = taskMap.keySet();
        System.out.println("==============Tasks==================");
        for (String key:keys
             ) {
            System.out.println(taskMap.get(key));
        }
        System.out.println("=====================================");
    }
}
